package com.bdc.riabankdeposit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RiAbankdepositApplication {

    public static void main(String[] args) {
        SpringApplication.run(RiAbankdepositApplication.class, args);
    }

}
